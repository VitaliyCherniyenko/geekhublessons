package geekhublessons.JsonSerializer.json.adapters;

import geekhublessons.JsonSerializer.json.JsonSerializer;
import org.json.JSONArray;
import org.json.JSONException;

import java.util.Collection;

/**
 * Converts all objects that extends java.util.Collections to JSONArray.
 */
public class CollectionAdapter implements JsonDataAdapter<Collection> {
    @Override
    public Object toJson(Collection c) throws JSONException{
        JSONArray array=new JSONArray();
       for(Object cl: c)  {
           array.put(JsonSerializer.serialize(cl));
       }
        return array.toString();
    }
}
