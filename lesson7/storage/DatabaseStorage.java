package geekhublessons.lesson7.storage;

import geekhublessons.lesson7.objects.Cat;
import geekhublessons.lesson7.objects.Entity;
import geekhublessons.lesson7.objects.Ignore;
import geekhublessons.lesson7.objects.User;

import java.lang.reflect.Field;
import java.sql.*;
import java.util.*;

/**
 * Implementation of {@link geekhublessons.lesson7.storage.Storage} that uses database as a storage for objects.
 * It uses simple object type names to define target table to save the object.
 * It uses reflection to access objects fields and retrieve data to map to database tables.
 * As an identifier it uses field id of {@link geekhublessons.lesson7.objects.Entity} class.
 * Could be created only with {@link java.sql.Connection} specified.
 */
public class DatabaseStorage implements Storage {
    private Connection connection;

    public DatabaseStorage(Connection connection) {
        this.connection = connection;
    }

    @Override
    public <T extends Entity> T get(Class<T> clazz, Integer id) throws Exception {
        String sql = "SELECT * FROM " + clazz.getSimpleName() + " WHERE id = " + id;
        Statement statement = connection.createStatement();
        List<T> result = extractResult(clazz, statement.executeQuery(sql));
        return result.isEmpty() ? null : result.get(0);

    }

    @Override
    public <T extends Entity> List<T> list(Class<T> clazz) throws Exception {
        ResultSet rs = connection.createStatement().executeQuery("SELECT * FROM " + clazz.getSimpleName());
        return extractResult(clazz, rs);
    }

    @Override
    public <T extends Entity> boolean delete(T entity) throws Exception {
        String sql = "DELETE from " + entity.getClass().getSimpleName() + " WHERE id =" + entity.getId();
        return connection.createStatement().execute(sql);
    }

    @Override
    public <T extends Entity> void save(T entity) throws Exception {
        Map<String, Object> data = prepareEntity(entity);
        int size = data.size();
        int count = data.size();

        String sql = null;
        if (entity.isNew()) {
            sql = "INSERT into " + entity.getClass().getSimpleName() + "( ";
            for (String s : data.keySet()) {
                System.out.println(s);
                sql += s;
                if (count > 1)
                    sql += ", ";
                count--;
            }
            ;
            sql += " )" + " VALUES ( ";
            for (String s : data.keySet()) {
                System.out.println(s + " " + data.get(s));
                if (data.get(s).equals(true) || data.get(s).equals(false)) {
                    sql += data.get(s);
                } else sql += "\"" + data.get(s) + "\"";
                if (size > 1)
                    sql += ", ";
                size--;
            }
            sql += " );";
            System.out.println(sql);
            connection.createStatement().execute(sql, Statement.RETURN_GENERATED_KEYS);
            entity.setId(updateId(entity));
        } else {
            for (String s : data.keySet()) {
                if (data.get(s).equals(true) || data.get(s).equals(false)) {
                    sql = "UPDATE " + entity.getClass().getSimpleName() + " SET " + s + "= " + data.get(s) + " WHERE "
                            + " id = " + entity.getId();
                } else
                    sql = "UPDATE " + entity.getClass().getSimpleName() + " SET " + s + "= \"" + data.get(s) + "\" WHERE "
                            + " id = " + entity.getId();
                System.out.println(sql);
                connection.createStatement().execute(sql);
            }
        }
    }

    private <T extends Entity> Map<String, Object> prepareEntity(T entity) throws Exception {
        Field[] fields = entity.getClass().getDeclaredFields();
        Map<String, Object> map = new LinkedHashMap<String, Object>();
        for (Field f : fields) {
            f.setAccessible(true);
            if (!f.isAnnotationPresent(Ignore.class))
                map.put(f.getName(), f.get(entity));
            f.setAccessible(false);
        }
        return map;
    }

    private <T extends Entity> List<T> extractResult(Class<T> clazz, ResultSet resultset) throws Exception {
        ArrayList<T> entities = new ArrayList<T>();
        while (resultset.next()) {
            T entity = clazz.newInstance();
            entity.setId(resultset.getInt(1));
            for (Field field : clazz.getDeclaredFields()) {
                field.setAccessible(true);
                if (field.isAnnotationPresent(Ignore.class)) continue;
                Object value = resultset.getObject(field.getName());
                field.set(entity, value);
                field.setAccessible(false);
            }
            entities.add(entity);
        }
        return entities;
    }

    private <T extends Entity> int updateId(T entity) throws SQLException, IllegalAccessException {
        Field[] fields = entity.getClass().getDeclaredFields();
        int size = fields.length;
        for (Field f : fields) {
            if (f.isAnnotationPresent(Ignore.class))
                size--;
        }
        System.out.println(size);
        int id;
        String sql = "SELECT id from " + entity.getClass().getSimpleName() + " WHERE ";
        for (Field f : fields) {
            if (!f.isAnnotationPresent(Ignore.class)) {
                f.setAccessible(true);
                if (f.get(entity).equals(true) || f.get(entity).equals(false)) {
                    sql += f.getName() + " = " + f.get(entity);
                } else
                    sql += f.getName() + " = \"" + f.get(entity) + "\" ";
                f.setAccessible(false);
                if (size > 1)
                    sql += " and ";
                size--;
            }
        }
        System.out.println(sql);
        ResultSet resultSet = connection.createStatement().executeQuery(sql);
        while (resultSet.next()) {
            id = resultSet.getInt(1);
            return id;
        }
        return 0;
    }
}
