package geekhublessons.lesson4.task2;

import java.util.Calendar;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Пользователь
 * Date: 04/11/13
 * Time: 19:55
 * To change this template use File | Settings | File Templates.
 */
public interface TaskManager {
    public void addTask(Calendar date, Task task);

    public void removeTask(Calendar date);

    public Collection<String> getCategories();

    //For next 3 methods tasks should be sorted by scheduled date
    public Map<String, List<Task>> getTasksByCategories();

    //Реализовать интерфейс
    public List<Task> getTasksByCategory(String category);

    public List<Task> getTasksForToday();
}
