package geekhublessons.lesson4.task2;

import java.util.Calendar;

/**
 * Created with IntelliJ IDEA.
 * User: Пользователь
 * Date: 04/11/13
 * Time: 19:56
 * To change this template use File | Settings | File Templates.
 */
public class Task {
    private String category;
    private String description;
    private Calendar date;

    public String toString() {
        return "Task category: " + this.getCategory() + ". Task description: " + this.getDescription() +
                " Task date: " + this.getDate().get(Calendar.DATE) + ":" + this.getDate().get(Calendar.MONTH) +
                ":" + this.getDate().get(Calendar.YEAR) + "\n";
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Calendar getDate() {
        return date;
    }

    public void setDate(Calendar date) {
        this.date = date;
    }
}
