package geekhublessons.lesson4.task2;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: Пользователь
 * Date: 04/11/13
 * Time: 20:18
 * To change this template use File | Settings | File Templates.
 */
public class Tasks implements TaskManager {
    public Tasks() {
        tasks = new ArrayList<Task>();
    }

    private Collection<Task> tasks;

    @Override
    public void addTask(Calendar date, Task task) {
        task.setDate(date);
        tasks.add(task);
    }

    @Override
    public void removeTask(Calendar date) {
        for (Task t : tasks) {
            if (t.getDate().equals(date))
                tasks.remove(t);
        }
    }

    @Override
    public Collection<String> getCategories() {
        List<String> categories = new ArrayList<String>();
        for (Task t : tasks) {
            if (categories.contains(t.getCategory()))
                continue;
            categories.add(t.getCategory());
        }
        return categories;
    }

    @Override
    public Map<String, List<Task>> getTasksByCategories() {
        Map<String, List<Task>> tasksByCategories = new HashMap<String, List<Task>>();
        for (Task t : tasks) {
            if (!tasksByCategories.containsKey(t.getCategory())) {
                continue;
            }
            tasksByCategories.put(t.getCategory(), getTasksByCategory(t.getCategory()));
        }


        return tasksByCategories;
    }

    @Override
    public List<Task> getTasksByCategory(String category) {
        List<Task> tasksByCategories = new ArrayList<Task>();
        for (Task t : tasks) {
            if (!t.getCategory().equals(category)) {
                continue;
            }
            tasksByCategories.add(t);
        }
        Collections.sort(tasksByCategories, new Comparator<Task>() {
            @Override
            public int compare(Task task, Task task2) {
                return task.getDate().compareTo(task2.getDate());
            }
        });
        return tasksByCategories;
    }

    @Override
    public List<Task> getTasksForToday() {
        GregorianCalendar today = new GregorianCalendar(
                new GregorianCalendar().get(Calendar.YEAR),
                new GregorianCalendar().get(Calendar.MONTH),
                new GregorianCalendar().get(Calendar.DATE)
        );
        List<Task> tasksForToday = new ArrayList<Task>();
        for (Task t : tasks) {
            if (!t.getDate().equals(today)) {
                continue;
            }
            tasksForToday.add(t);
        }
        return tasksForToday;
    }

    public Collection<Task> getTasks() {
        return tasks;
    }

    public void setTasks(Collection<Task> tasks) {
        this.tasks = tasks;
    }
}
