package geekhublessons.lesson4.task2;

import java.util.GregorianCalendar;

/**
 * Created with IntelliJ IDEA.
 * User: Пользователь
 * Date: 12/11/13
 * Time: 20:46
 * To change this template use File | Settings | File Templates.
 */
public class Main {
    public static void main(String[] args) {
        Task task1 = new Task();
        task1.setCategory("urgent");

        Task task2 = new Task();
        task2.setCategory("urgent");

        Task task3 = new Task();
        task3.setCategory("non-urgent");

        Tasks tasks = new Tasks();
        tasks.addTask(new GregorianCalendar(2013, 10, 12), task1);
        tasks.addTask(new GregorianCalendar(2013, 10, 15), task2);
        tasks.addTask(new GregorianCalendar(2013, 10, 15), task3);

        System.out.println(tasks.getCategories());
        System.out.println(tasks.getTasksByCategory("urgent"));
    }
}
