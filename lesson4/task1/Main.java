package geekhublessons.lesson4.task1;

import java.util.HashSet;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: Пользователь
 * Date: 12/11/13
 * Time: 20:24
 * To change this template use File | Settings | File Templates.
 */
public class Main {
    public static void main(String[] args) {
        Set<String> primitiveTypes=new HashSet<String>();
        primitiveTypes.add("int");
        primitiveTypes.add("double");
        primitiveTypes.add("boolean");

        Set<String> types=new HashSet<String>();
        types.add("boolean");
        types.add("String");
        types.add("Character");

        SetOperationsImpl setOperations =new SetOperationsImpl();

        System.out.println(setOperations.symmetricSubtract(primitiveTypes,types));
    }
}
