package geekhublessons.lesson4.task1;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: Пользователь
 * Date: 03/11/13
 * Time: 23:57
 * To change this template use File | Settings | File Templates.
 */
public class SetOperationsImpl implements SetOperations {

    @Override
    public boolean equals(Set a, Set b) {
        return a.equals(b);
    }

    @Override
    public Set union(Set a, Set b) {
        a.addAll(b);
        return a;

    }

    @Override
    public Set subtract(Set a, Set b) {
        boolean add=a.removeAll(b);
        return a;
    }

    @Override
    public Set intersect(Set a, Set b) {
        Set<String> set=new HashSet<String>();
        for (Object p:a){
            if (b.contains(p))
                set.add((String)p);

        }
        return set;
    }

    @Override
    public Set symmetricSubtract(Set a, Set b) {
        Set<String> set= intersect(a,b);
        union(a, b);
        a.removeAll(set);
        return a;
    }

}
