package geekhublessons.lesson4.task1;

import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: Пользователь
 * Date: 03/11/13
 * Time: 21:53
 * To change this template use File | Settings | File Templates.
 */
public interface SetOperations{
    public boolean equals(Set a, Set b);
    public Set union(Set a, Set b);
    public Set subtract(Set a, Set b);
    public Set intersect(Set a, Set b);
    public Set symmetricSubtract(Set a, Set b);
}
