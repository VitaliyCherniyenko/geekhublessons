package geekhub.lessons.lesson3;

import geekhub.lessons.lesson2.vehicles.Car;
import geekhub.lessons.lesson2.vehicles.Vehicles;
import geekhub.lessons.lesson3.exceptions.CarException;
import geekhub.lessons.lesson3.exceptions.MaximumSpeedException;
import geekhub.lessons.lesson3.exceptions.NotEnoughFuelException;

import java.util.Scanner;

import static java.lang.String.valueOf;

/**
 * Created with IntelliJ IDEA.
 * User: capit
 * Date: 26.10.13
 * Time: 15:04
 * To change this template use File | Settings | File Templates.
 */
public class Menu {
    private Vehicles vehicles;
    private Car car;

    public Menu() {
        vehicles = new Vehicles();
    }

    public void showMenu() {
        System.out.println("1 - Create new Car");
        System.out.println("2 - Show all cars");
        System.out.println("3 - Select car");
        System.out.println("4 - Exit");
        System.out.println("Make your choice");
        makeChoice(readInt());
    }

    public void makeChoice(int i) {
        switch (i) {
            case 1:
                try {
                    createCar();
                } catch (CarException exeption) {
                    exeption.printStackTrace();
                }
                break;
            case 2:
                vehicles.sortByPrice();
                showMenu();
                break;
            case 3:
                System.out.println("Select car of");
                for (int j = 0; j < vehicles.getVehicles().size(); j++) {
                    System.out.println(j + 1 + " " + vehicles.getVehicles().get(j));
                }
                try {
                    selectCar();
                } catch (CarException e) {
                    e.printStackTrace();
                }

                break;
            case 4:
                System.exit(0);
                break;
            default:
                System.out.println("incorrect choice. Try again.");
                makeChoice(readInt());
                break;
        }
    }

    public void selectCar() throws CarException {
        if (vehicles.getVehicles().size() == 0) {
            System.out.println("You haven't cars. Create car please.");
            this.createCar();
        }
        int carId = readInt();
        if (carId < vehicles.getVehicles().size() || carId > vehicles.getVehicles().size()) {
            System.out.println("Error. Enter car number.");
            carId = readInt();
        }
        car = (Car) vehicles.getVehicles().get(carId - 1);
        System.out.println("What you want to do?");
        carMenu();

    }

    public void carMenu() {
        System.out.println("1 - Go");
        System.out.println("2 - Brake");
        System.out.println("3 - Turn");
        System.out.println("4 - Load GasTank");
        System.out.println("5 - to main menu");
        System.out.println("6 - what I can do with car");
        carMenuChoice(readInt());
    }

    public void carMenuChoice(int i) {
        switch (i) {
            case 1:
                car.accelerate();
                carMenuChoice(readInt());
                break;
            case 2:
                car.brake();
                carMenuChoice(readInt());
                break;
            case 3:
                car.turn();
                carMenuChoice(readInt());
                break;
            case 4:
                car.loadGasTank();
                carMenuChoice(readInt());
                break;
            case 5:
                this.showMenu();
                break;
            case 6:
                carMenu();
                break;
            default:
                System.out.println("Incorrect choice. Try again.");
                carMenu();
                break;
        }
    }

    public void createCar() throws CarException {
        System.out.println("Creating a car");
        car = new Car(0, 0);
        System.out.println("Enter car's model ");
        car.setModel(readString());
        System.out.println("Enter max car speed");
        car.getEngin().setMaxSpeed(readSpeed());
        System.out.println("Enter gasTank max fuel");
        car.getGasTank().setMaxFuel(readFuel());
        System.out.println("Enter car's price");
        car.setPrice(readInt());
        System.out.println("The car was created. What do you want to do?");
        vehicles.addVehicle(car);
        carMenu();
    }

    public String readString() {
        Scanner scanner = new Scanner(System.in);
        String line = "";
        if (scanner.hasNext())
            line = scanner.nextLine();
        return line;
    }

    public int readInt() {
        Scanner scanner = new Scanner(System.in);
        int choice = 0;
        if (scanner.hasNext())
            try {
                choice = Integer.parseInt(scanner.nextLine());
            } catch (Exception e) {
                System.out.println("Incorrect. Try again.");
                readInt();
            }
        return choice;
    }

    public int readFuel() throws NotEnoughFuelException {
        Scanner scanner = new Scanner(System.in);
        int maxFuel = 0;
        if (scanner.hasNext())
            try {
                maxFuel = Integer.parseInt(scanner.nextLine());
                if ((maxFuel < 3) || (maxFuel > 50)) {
                    throw new NotEnoughFuelException();
                }
            } catch (Exception e) {
                System.out.println("Incorrect. Try again.");
                readInt();
            }
        return maxFuel;
    }

    public int readSpeed() {
        Scanner scanner = new Scanner(System.in);
        int maxSpeed = 0;
        if (scanner.hasNext())
            try {
                maxSpeed = Integer.parseInt(scanner.nextLine());
                if ((maxSpeed < 3) || (maxSpeed > 300)) {
                    throw new MaximumSpeedException();
                }
            } catch (Exception e) {
                System.out.println("Incorrect. Try again.");
                readInt();
            }
        return maxSpeed;
    }

    public Vehicles getVehicles() {
        return vehicles;
    }

    public void setVehicles(Vehicles vehicles) {
        this.vehicles = vehicles;
    }
}
