package geekhub.lessons.lesson2;

/**
 * Created with IntelliJ IDEA.
 * User: capit
 * Date: 28.10.13
 * Time: 17:24
 * To change this template use File | Settings | File Templates.
 */
public class TestStrings {
    public static long testString(){
        long first = System.nanoTime();
        String s = "test";
        for (int i=0; i<1000000000;i++){
            s+=s;
        }
        return System.nanoTime()-first;
    }

    public static long testStringBuilder(){
        long first = System.nanoTime();
        StringBuilder s = new StringBuilder("test");
        for (int i=0; i<1000000000;i++){
            s.append(s);
        }
        return System.nanoTime()-first;
    }

    public static long testStringBuffer(){
        long first = System.nanoTime();
        StringBuffer s = new StringBuffer("test");
        for (int i=0; i<1000000000;i++){
            s.append(s);
        }
        return System.nanoTime()-first;
    }
}
