package geekhub.lessons.lesson3;


import geekhub.lessons.lesson3.Menu;
import geekhub.lessons.lesson2.vehicles.Car;
import geekhub.lessons.lesson2.vehicles.Vehicle;
import geekhub.lessons.lesson2.vehicles.Vehicles;

/**
 * Created with IntelliJ IDEA.
 * User: capit
 * Date: 25.10.13
 * Time: 19:06
 * To change this template use File | Settings | File Templates.
 */
public class Main {
    public static void main(String[] args) {
        Vehicles vehicles = new Vehicles();

        Vehicle car = new Car(300, 50);
        car.setPrice(40000);
        car.setModel("Porshe");

        Vehicle car1 = new Car(310, 45);
        car1.setPrice(60000);
        car1.setModel("Ferrari");

        Vehicle car2 = new Car(180, 40);
        car2.setPrice(50000);
        car2.setModel("Chevrolet");

        Vehicle[] cars = {car, car1, car2};
        Vehicle[] sortedCars = (Vehicle[]) vehicles.sort(cars);

        System.out.println(cars[1]);
        System.out.println(sortedCars[1]);

        vehicles.addVehicle(car);
        vehicles.addVehicle(car1);
        vehicles.addVehicle(car2);

        vehicles.sortByPrice();
        vehicles.sortByModel();

        System.out.println("\n");
        Menu menu = new Menu();
        menu.showMenu();
    }
}
