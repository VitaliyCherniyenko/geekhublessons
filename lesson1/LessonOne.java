package geekhub.lessons.lesson1;


import java.util.Scanner;

public class LessonOne {

    private int fibonacciNumber;
    private int factorial;


    public LessonOne(int fn, int f) {
        this.setFibonacciNumber(fn);
        this.setFactorial(f);
    }


    public static void main(String[] args) {
        int factorial = 0;
        int fibonacciNumber = 0;

        System.out.println("Enter data > 2");
        Scanner scanner = new Scanner(System.in);
        if (scanner.hasNext())
            try {
                factorial = LessonOne.validateInt(scanner.nextLine());
            } catch (NumberFormatException e) {
                System.out.println("Incorrect data");
            }

        System.out.println("Enter int(>2) ");
        scanner = new Scanner(System.in);
        if (scanner.hasNext())
            try {
                fibonacciNumber = LessonOne.validateInt(scanner.nextLine());
            } catch (NumberFormatException e) {
                System.out.println("Incorrect data");
            }


        LessonOne l = new LessonOne(factorial, fibonacciNumber);
        System.out.println("The " + l.getFibonacciNumber() + "th Fibonachi number is: "
                + l.findFibonachiNumber());

        System.out.println("The factorial of " + l.getFactorial() + " is: " + l.findFactorial());
    }


    public static int validateInt(String s) throws NumberFormatException {
        int value = 0;

        value = Integer.parseInt(s);
        if (value <= 2) {
            System.out.println("This data is not correct");
            System.exit(0);
        }


        return value;
    }

    //This method finds the fn element of Fibonacci numbers ;
    public int findFibonachiNumber() {
        int result = 0;
        int fn2 = 1;
        int fn1 = 0;
        for (int i = 3; i <= fibonacciNumber; i++) {
            result = fn1 + fn2;
            fn1 = fn2;
            fn2 = result;
        }

        return result;
    }

    //This method find factorial of f ;
    public int findFactorial() {
        int result = 1;
        for (int i = 1; i <= factorial; i++) {
            result = result * i;
        }
        return result;
    }


    public int getFibonacciNumber() {
        return fibonacciNumber;
    }

    public void setFibonacciNumber(int fibonacciNumber) {
        this.fibonacciNumber = fibonacciNumber;
    }

    public int getFactorial() {
        return factorial;
    }

    public void setFactorial(int factorial) {
        this.factorial = factorial;
    }


}
