package geekhub.lessons.lesson2.vehicles;

/**
 * Created with IntelliJ IDEA.
 * User: capit
 * Date: 25.10.13
 * Time: 18:36
 * To change this template use File | Settings | File Templates.
 */
public abstract class Vehicle implements Drivable, Comparable<Vehicle> {
    private String model;
    private double price;

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public abstract String getDescription();

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }
}
