package geekhub.lessons.lesson2.vehicles;

/**
 * Created with IntelliJ IDEA.
 * User: capit
 * Date: 25.10.13
 * Time: 18:39
 * To change this template use File | Settings | File Templates.
 */
public interface Drivable {
    public void accelerate();

    public void brake();

    public void turn();
}
