package geekhub.lessons.lesson2.vehicles;

import geekhub.lessons.lesson2.hardware.Engin;
import geekhub.lessons.lesson2.hardware.GasTank;
import geekhub.lessons.lesson2.hardware.Wheels;

/**
 * Created with IntelliJ IDEA.
 * User: capit
 * Date: 25.10.13
 * Time: 18:41
 * To change this template use File | Settings | File Templates.
 */
public class Car extends Vehicle {
    private GasTank gasTank;
    private Engin engin;
    private Wheels wheels;

    public Car(int maxSpeed, int maxFuel) {
        gasTank = new GasTank(maxFuel);
        engin = new Engin(gasTank);
        wheels = new Wheels(engin);

        gasTank.setMaxFuel(maxFuel);
        engin.setMaxSpeed(maxSpeed);
    }

    public void loadGasTank() {
        System.out.println("Loading gasTanks...");
        gasTank.setCurrentFuel(gasTank.getMaxFuel());
    }

    @Override
    public String getDescription() {
        return "This is car";
    }

    @Override
    public void accelerate() {
        wheels.pushingGasPedal();
    }

    @Override
    public void brake() {
        wheels.pushingBrakePedal();
    }

    @Override
    public void turn() {
        System.out.println("Turning....");
    }

    public String toString() {
        return "Car: " + this.getModel() + " " + this.getPrice();
    }

    @Override
    public int compareTo(Vehicle o) {
        if (this.getPrice() > o.getPrice()) return 1;
        if (this.getPrice() < o.getPrice()) return -1;
        return 0;
    }

    public GasTank getGasTank() {
        return gasTank;
    }

    public void setGasTank(GasTank gasTank) {
        this.gasTank = gasTank;
    }

    public Engin getEngin() {
        return engin;
    }

    public void setEngin(Engin engin) {
        this.engin = engin;
    }

    public Wheels getWheels() {
        return wheels;
    }

    public void setWheels(Wheels wheels) {
        this.wheels = wheels;
    }


}
