package geekhub.lessons.lesson2.vehicles;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created with IntelliJ IDEA.
 * User: capit
 * Date: 25.10.13
 * Time: 18:45
 * To change this template use File | Settings | File Templates.
 */
public class Vehicles {
    private ArrayList<Vehicle> vehicles = new ArrayList<Vehicle>();

    public  Comparable[] sort(Comparable[] comparables){
        Comparable[] aComparables=comparables.clone();
        Arrays.sort(aComparables);
        return aComparables;
    }

    public void addVehicle(Vehicle vehicle) {
        vehicles.add(vehicle);
    }

    public void sortByPrice() {
        System.out.println("\n" + "Vehicles sorted by price");
        Collections.sort(vehicles, new Comparator<Vehicle>() {
            @Override
            public int compare(Vehicle o1, Vehicle o2) {
                return (int) (o1.getPrice() - o2.getPrice());
            }
        });
        for (Vehicle v : vehicles)
            System.out.println(v);
    }

    public void sortByModel() {
        System.out.println("\n" + "Vehicles sorted by model");
        Collections.sort(vehicles, new Comparator<Vehicle>() {
            @Override
            public int compare(Vehicle o1, Vehicle o2) {
                return o1.getModel().compareTo(o2.getModel());
            }
        });
        for (Vehicle v : vehicles)
            System.out.println(v);
    }

    public ArrayList<Vehicle> getVehicles() {
        return vehicles;
    }

    public void setVehicles(ArrayList<Vehicle> vehicles) {
        this.vehicles = vehicles;
    }
}
