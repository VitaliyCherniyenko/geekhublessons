package geekhub.lessons.lesson2.hardware;

/**
 * Created with IntelliJ IDEA.
 * User: capit
 * Date: 26.10.13
 * Time: 0:36
 * To change this template use File | Settings | File Templates.
 */
public class Engin {
    public Engin(GasTank gt) {
        setGasTank(gt);
        setCurrentSpeed(0);
    }

    private GasTank gasTank;
    private int currentSpeed;
    private int maxSpeed;

    public boolean checkSpeed() {
        if (currentSpeed >= maxSpeed) {
            System.out.println("You cant move faster. Max speed is complete.");
            return false;
        }
        return true;
    }

    public boolean checkGasTank() {
        if (gasTank.getCurrentFuel() <= 0) {
            System.out.println("You cant move. You must load gastTank");
            return false;
        }
        return true;
    }

    public GasTank getGasTank() {
        return gasTank;
    }

    public void setGasTank(GasTank gasTank) {
        this.gasTank = gasTank;
    }

    public int getCurrentSpeed() {
        return currentSpeed;
    }

    public void setCurrentSpeed(int currentSpeed) {
        this.currentSpeed = currentSpeed;
    }

    public int getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(int maxSpeed) {
        this.maxSpeed = maxSpeed;
    }
}
