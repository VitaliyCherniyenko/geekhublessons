package geekhub.lessons.lesson2.hardware;

/**
 * Created with IntelliJ IDEA.
 * User: capit
 * Date: 26.10.13
 * Time: 0:36
 * To change this template use File | Settings | File Templates.
 */
public class GasTank {
    private double currentFuel;
    private double maxFuel;

    public GasTank(double maxFuel) {
        this.maxFuel = maxFuel;
    }

    public double getCurrentFuel() {
        return currentFuel;
    }

    public void setCurrentFuel(double currentFuel) {
        this.currentFuel = currentFuel;
    }

    public double getMaxFuel() {
        return maxFuel;
    }

    public void setMaxFuel(double maxFuel) {
        this.maxFuel = maxFuel;
    }
}
