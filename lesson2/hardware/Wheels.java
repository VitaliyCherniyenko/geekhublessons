package geekhub.lessons.lesson2.hardware;

/**
 * Created with IntelliJ IDEA.
 * User: capit
 * Date: 26.10.13
 * Time: 0:13
 * To change this template use File | Settings | File Templates.
 */
public class Wheels {
    private Engin engin;

    public Wheels(Engin e) {
        engin = e;
    }

    public void pushingGasPedal() {
        if (engin.checkGasTank()) {
            if (!engin.checkSpeed()) {
                return;
            }
            engin.setCurrentSpeed(engin.getCurrentSpeed() + 10);
            engin.getGasTank().setCurrentFuel(engin.getGasTank().getCurrentFuel() - 10);
            System.out.println("Going...");
        }

    }

    public void pushingBrakePedal() {
        if (engin.getCurrentSpeed() <= 0) {
            System.out.println("You don't move.");
        } else {
            System.out.println("Braking..");
            engin.setCurrentSpeed(engin.getCurrentSpeed() - 10);
        }

    }

    public Engin getEngin() {
        return engin;
    }

    public void setEngin(Engin engin) {
        this.engin = engin;
    }
}
