package geekhublessons.threads;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;

/**
 * Utils class that contains useful method to interact with URLConnection
 */
public class ConnectionUtils {

    /**
     * Downloads content for specified URL and returns it as a byte array.
     * Should be used for small files only. Don't use it to download big files it's dangerous.
     *
     * @param url
     * @return
     * @throws IOException
     */
    public static byte[] getData(URL url) throws IOException {
        ByteArrayOutputStream bous = new ByteArrayOutputStream();
        URLConnection connection = url.openConnection();
        InputStream reader = new BufferedInputStream(connection.getInputStream());
        byte[] content = new byte[1];
        while (-1 != reader.read(content)) {
            bous.write(content);
        }
        byte [] result = bous.toByteArray();
        return result;
    }
}
