package geekhublessons.translator.source;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 * Implementation for loading content from specified URL.<br/>
 * Valid paths to load are http://someurl.com, https://secureurl.com, ftp://frpurl.com etc.
 */
public class URLSourceProvider implements SourceProvider {

    @Override
    public boolean isAllowed(String pathToSource) {
        try {
            URL url = new URL(pathToSource);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.connect();
            int rc = connection.getResponseCode();
            if (rc == 200) {
                return true;
            }
        } catch (IOException e) {
            return false;
        }
        return false;
    }

    @Override
    public String load(String pathToSource) throws IOException {
        StringBuilder sb = new StringBuilder();
        if (isAllowed(pathToSource)) {
            URL url = new URL(pathToSource);
            URLConnection connection = url.openConnection();
            connection.connect();
            InputStreamReader reader = new InputStreamReader(connection.getInputStream(), "utf-8");
            BufferedReader br = new BufferedReader(reader);
            String line = null;
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
        }
        return sb.toString();
    }
}
