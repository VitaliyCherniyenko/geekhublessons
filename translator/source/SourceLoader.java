package geekhublessons.translator.source;

import sun.org.mozilla.javascript.internal.json.JsonParser;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * SourceLoader should contains all implementations of SourceProviders to be able to load different sources.
 */
public class SourceLoader {
    private List<SourceProvider> sourceProviders = new ArrayList<SourceProvider>();

    public SourceLoader() {
        sourceProviders.add(new URLSourceProvider());
        sourceProviders.add(new FileSourceProvider());
    }

    public String loadSource(String pathToSource) throws IOException {
        for (SourceProvider s : sourceProviders) {
            if (s.isAllowed(pathToSource)) {
                return s.load(pathToSource);
            }
        }
        throw new IOException();
    }

    public boolean checkPath(String pathToSource) throws IOException{
        for (SourceProvider s : sourceProviders) {
            if (s.isAllowed(pathToSource)) {
                return true;
            }
        }
        return false;
    }
}
