package geekhublessons.translator;

import geekhublessons.translator.source.SourceLoader;
import geekhublessons.translator.source.URLSourceProvider;

import java.io.IOException;
import java.util.Scanner;

public class TranslatorController {

    public static void main(String[] args) throws IOException {
        //initialization
        SourceLoader sourceLoader = new SourceLoader();
        Translator translator = new Translator(new URLSourceProvider());

        Scanner scanner = new Scanner(System.in);
        String command = scanner.next();
        while (!sourceLoader.checkPath(command)) {
            System.out.println("Wrong path to source. Enter correct source.");
            command = scanner.next();
        }

        while (!"exit".equals(command)) {
            while (!sourceLoader.checkPath(command)) {
                if (command.equals("exit")) System.exit(0);
                System.out.println("Wrong path to source. Enter correct path.");
                command = scanner.next();
            }
            String source = sourceLoader.loadSource(command);
            String translation = translator.translate(source);

            System.out.println("Original: " + source);
            System.out.println("Translation: " + translation);

            command = scanner.next();
        }
    }
}
